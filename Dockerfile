FROM alpine:3

RUN ["apk", "add", "curl"]

ENV UUID="replaceme"

# https://healthchecks.io/docs/bash/
# https://docs.docker.com/engine/reference/builder/#run
CMD ["sh", "-c", "curl -m 10 --retry 2 --fail --silent --show-error -o /dev/null https://hc-ping.com/$UUID"]
